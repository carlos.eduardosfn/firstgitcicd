package br.com.ciapp.ciapp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/v1/firstapp")
public class GetHello {

    @GetMapping("helloWord")
    public String helloWord() {
        return "Hellor word";
    }
}
